
procedure AndThenNot is
begin
   if Toto and then not
     True
   then
     null;
   end if;

   if Toto
     and then not
       True
   then
     null;
   end if;

   if Toto
     or else not
       True
   then
     null;
   end if;

end AndThenNot;
